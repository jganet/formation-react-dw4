import React, { Component } from "react";
import PropTypes from 'prop-types';
import "./Table.css";
import CardDeck from "../../components/CardDeck";

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            players : [],
            decks : []
        };
    }

    handleDrop() {

    }


  render() {

    return (
        <div class="table">
            <CardDeck 
                player="player1" 
                visible={this.props.started} 
                deck={this.props.deckPlayer1}
                shuffle>
            </CardDeck>
            <CardDeck 
                player="player2" 
                visible={this.props.started} 
                deck={this.props.deckPlayer2}
                shuffle>
            </CardDeck>
        </div>
    );
  }
}

export default Table;
