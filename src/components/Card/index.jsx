import React, { Component } from "react";
import PropTypes from 'prop-types';
import "./Card.css";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
        visible : false
    };

    this.return = this.return.bind(this);
  }

  return() {
    this.setState({
        visible : !this.state.visible
    });
  }

  render() {
    let trueColor  = (this.props.color === '♥' || this.props.color === '♦') ? 'red' : 'black';

    return (
        <div className={`card ${this.props.player}`} style={{color : trueColor}} onClick={this.return}>
            {(this.state.visible) ? 
            <div className="content front">
                <div className='value left-top'>
                    <div>{this.props.color}</div>
                    <div>{this.props.value}</div>
                </div>
                <div className='value center'>
                    <div>{(this.props.value === 'A') ? this.props.color : this.props.value}</div>
                </div>
                <div className='value right-bottom'>
                    <div>{this.props.color}</div>
                    <div>{this.props.value}</div>
                </div>
            </div> :
            <div className="content back">
            </div>}
        </div>
    );
  }
}

Card.propTypes = {
    color : PropTypes.string,
    value : PropTypes.string
} 

export default Card;
