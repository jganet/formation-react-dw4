import React, { Component } from "react";
import PropTypes from 'prop-types';
import "./PrettyButton.css";

const PrettyButton = ({children, onClick}) => {
    return (
        <div className="button pretty-button" onClick={onClick}>
            {children}
        </div>
    )
}

export default PrettyButton;
