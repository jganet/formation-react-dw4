import React, { Component } from "react";
import PropTypes from 'prop-types';
import "./CardDeck.css";
import Card from "../Card";

class CardDeck extends Component {
  constructor(props) {
    super(props);

    this.state = {
        cards : [],
    };
  }


  componentWillMount() {
    console.log('before render');

    let cards = [];

    
    let deckPlayers = [[], []];

    if(this.props.colors) {
        for(var i = 0; i < this.props.colors.length; i++) {
            for(var j = 0; j < this.props.values.length; j++) {
                console.log(this.props.colors[i], this.props.values[j]);

                cards.push(<Card 
                    key={this.props.colors[i]+this.props.values[j]} 
                    color={this.props.colors[i]} 
                    value={this.props.values[j]} />);

                let randomPlayer = Math.round(Math.random());
                deckPlayers[randomPlayer].push(this.props.colors[i]+this.props.values[j]);
            }
        }
    }

    if(this.props.deck) {
        for(var i in this.props.deck) {
            let value = this.props.deck[i].substr(1);
            cards.push(<Card 
                key={this.props.deck[i]} 
                color={this.props.deck[i][0]} 
                value={value}
                player={this.props.player} />);
        }
    }
    
    this.setState({
        cards : cards,
        player1 : deckPlayers[0],
        player2 : deckPlayers[1]
    });
  }

  componentDidMount() {
    console.log('after render');
    this.read();
  }

  componentDidUnMount() {
    console.log('after unmount');
  }


  read() {
    console.log('read', this.state);
  }


  // next() {

  // }

  // random() {
  //   for(var i = 0; i < this.state.cards.length; i++) {
  //       console.log(this.state.cards[Math.ceil(Math.random()*this.state.cards.length)])
  //   }
  // }

  // give() {

  // }

    shuffleArray(array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        return array;
    }

  render() {
    let shuffleCards = this.shuffleArray(this.state.cards);
    return (
        <div className={`card-deck card-deck-${this.props.player}`} style={{'display ' : (this.props.visible ? 'block' : 'none')}}> 
            {shuffleCards}
        </div>
    );
  }
}

CardDeck.propTypes = {
    colors : PropTypes.array,
    values : PropTypes.array
} 

export default CardDeck;
