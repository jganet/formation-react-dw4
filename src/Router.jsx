import React, { Component, Fragment } from 'react';
import { Route } from 'react-router-dom'
import Home from './pages/Home';
import Lobby from './pages/Lobby';
import Game from './pages/Game';
import User from './pages/User';
import Stats from './pages/Stats';

class Router extends Component {
  render() {
    return (
      <Fragment>
          <Route exact path="/" component={Home} /> 
          <Route exact path="/lobby" component={Lobby} /> 
          <Route exact path="/game" component={Game} /> 
          <Route exact path="/user" component={User} /> 
          <Route exact path="/stats" component={Stats} /> 
      </Fragment>
    );
  }
}

export default Router;
