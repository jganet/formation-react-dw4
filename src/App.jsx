import React, { Component } from 'react';

import './App.css';
import { BrowserRouter } from 'react-router-dom'
import  Router  from './Router';
import  Layout  from './Layout';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn : false,
      token : null,
      user :null,
      username : '',
      password : '',
    }

  }

  componentWillMount() {
    if ( localStorage.getItem('token') ) {
      this.setState({'token' : JSON.parse(localStorage.getItem('token'))});      
    }
  }

  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Layout>
            <Router />
          </Layout>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
