import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { Grid, Row, Col  } from "react-bootstrap";
import "./index.css";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      containers : [
        "/",
        "/game",
        "/lobby",
        "/stats",
        "/user"
      ],
      path: null
    }

  }

  componentWillMount() {
    console.log(this.state);
  }


  render() {

    let indexOfCurentPathname = this.state.containers.indexOf(window.location.pathname);
    let prevLinkUrl = (indexOfCurentPathname === 0) ? this.state.containers[this.state.containers.length - 1] : this.state.containers[(indexOfCurentPathname - 1)];
    let nextLinkUrl = this.state.containers[(indexOfCurentPathname + 1)%this.state.containers.length];

    return (
      <Grid className="footer">
        <Row className="show-grid">
          <Col xs={4} md={4}>
            <Link to={prevLinkUrl || '/'}>
            prev
            </Link>
          </Col>
          <Col xs={4} md={4}>
            <p>Current page path : {window.location.pathname}</p>
          </Col>
          <Col xs={4} md={4}>
            <Link to={nextLinkUrl || '/'}>
            next
            </Link>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Footer;
