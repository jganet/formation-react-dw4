import { Component } from "react";
import "./index.css";

class Body extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      this.props.children
    );
  }
}

export default Body;
