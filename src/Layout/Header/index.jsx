import React, { Component } from "react";
import Login from "../../pages/User/Login";
import Me from "../../pages/User/Me";
import "./index.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      checked: false
    }
    this.onChecked = this.onChecked.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  componentWillMount() {
    console.log(this.state);
  }

  onChecked(error, response) {
    error ? this.setState({'checked' : false}):
    this.setState({'checked' : true})
  }

  onLogin(status) {
    this.setState({'loggedIn' : status});
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
        <header className="header">
          <img src={this.props.logo} className="header-logo" alt="logo" />
          {this.state.loggedIn ? (
            <Me onChecked={this.onChecked} />
          ) : (
            <Login onLogin={this.onLogin} />
          )}
        </header>
    );
  }
}

export default Header;
