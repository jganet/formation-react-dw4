import React, { Component, Fragment } from "react";
import logo from '../logo.svg';
import Header from './Header';
import Body from './Body';
import Footer from './Footer';

class Layout extends Component {
  render() {
    return (
      <Fragment>
        <Header logo={logo} />
        <Body>
          {this.props.children}
        </Body>
        <Footer/>
      </Fragment>
    );
  }
}

export default Layout;
