import React, { Component } from 'react';

class Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token : null,
      user :null
    }

  }

  componentWillMount() {
    console.log(this.state);
    this.setState({'token' :JSON.parse(localStorage.getItem('token'))}, () => this.checkMe(this.state.token.userId));
    
  }


  checkMe(userId) {
    fetch(`/api/Users/${userId}?access_token=${this.state.token.id}`, {})
    .then((res) => {
      console.log(res);
      return res.json();
    })
    .then((data) => {
      this.setState({'user':data});
      this.props.onChecked(null, data);
      console.log(data);
    });
  }

  render() {
    return (
      <div className="check-me">
          {this.state.user ? 
          <h1 className="check-me-title">Welcome {this.state.user.username}</h1>
          : null
          }
      </div>
    );
  }
}

export default Me;
