import React, { Component } from "react";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token : null,
      username : '',
      password : '',
    };

    this.onChange = this.onChange.bind(this);
    this.onLogin = this.onLogin.bind(this);
  }

  componentWillMount() {
    console.log(this.state);
  }

  onLogin() {
    fetch('/api/Users/login', {
      method : 'POST',
      body : JSON.stringify({'username': this.state.username, 'password' : this.state.password}),
      headers : {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
      }
    })
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      this.setState({'token' : data});
      localStorage.setItem('token', JSON.stringify(data));
      this.props.onLogin(true) ;
    });
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    return (
     <div className="login">
      <input onChange={this.onChange} value={this.state.username} name="username" type="text" placeholder="Username" />
      <input onChange={this.onChange} value={this.state.password} name="password" type="password"  placeholder="Password" />
      <button onClick={this.onLogin}>
        Login
      </button>
    </div>
    );
  }
}

export default Login;
