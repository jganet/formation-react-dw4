import React, { Component, Fragment} from "react";
import CardDeck from "../../components/CardDeck";
import PrettyButton from "../../components/PrettyButton";
import Table from "../../components/Table";

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
        gameStatus : 'wait'
    };

    this.startTheGame = this.startTheGame.bind(this);
    this.toggleTheGame = this.toggleTheGame.bind(this);
    this.initialDeckRef = React.createRef();

  }

  toggleTheGame () {
    //Toggle === start --> stop || stop --> start 
    
    let newStatus = this.state.gameStatus === 'started' ? 'stopped' : 'started';

    console.log('Game ' + newStatus);
    console.log('init Decks', this.initialDeckRef);

    this.setState({
        gameStatus : newStatus,
        player1 : Object.assign({}, this.initialDeckRef.current.state.player1),
        player2 : Object.assign({}, this.initialDeckRef.current.state.player2)
    });
  }

  startTheGame () {
    //Log your code for better comprehension

    

    this.setState({
        gameStatus : 'started',
        player1 : Object.assign({}, this.initialDeckRef.current.state.player1),
        player2 : Object.assign({}, this.initialDeckRef.current.state.player2)
    });
  }

  render() {
    return (
      <Fragment>
            <PrettyButton onClick={this.toggleTheGame} >
                {this.state.gameStatus === 'started' ? 'Stop' : 'Start'} the game
            </PrettyButton>

            {/*<PrettyButton onClick={this.startTheGame} >
                    {this.state.gameStatus === 'started' ? 'Stop' : 'Start'} the game
            </PrettyButton> */}

            {/*<PrettyButton onClick={this.startTheGame} >
                Start the game
            </PrettyButton> */}

            {/*<PrettyButton onClick={this.startTheGame} >
                Start the game
            </PrettyButton> */}

            {/*<CardDeck visible={() => this.state.gameStatus === 'started'} 
                colors={['♣', '♦', '♥', '♠']} 
                values={['7', '8', '9', '10', 'V', 'D', 'R', 'A']} />*/}


            <CardDeck
                ref={this.initialDeckRef} 
                visible={false} 
                colors={['♣', '♦', '♥', '♠']} 
                values={['7', '8', '9', '10', 'V', 'D', 'R', 'A']}
            />


            {
                (this.state.gameStatus === 'started') ? 
                    <Table 
                        started={() => this.state.gameStatus === 'started'} 
                        deckPlayer1={this.state.player1}
                        deckPlayer2={this.state.player2} 
                    >
                    </Table>
                : null  
            }
      </Fragment>
    );
  }
}

export default Game;
